<?php
session_start();

if (tokenExists()) {
    displayContent();
}
else{
    if (isFormSubmitted()){
        if (isFormValid()){
            defineToken();
            redirect();
        }else{
            displayError();
            displayForm();
        }
    }
    else{
        displayForm();
    }
}







function redirect(){
    header('Location: post.php');
}

function defineToken(): void {
    if (isRememberMe()){
        setcookie('token',"1",time() + 30 * 24 *60 * 60,"","",false,true);
    }
    else{
        $_SESSION['token'] = 1;
    }
}

function isRememberMe(){
    return isset($_POST['rememberMe']) && $_POST['rememberMe'] === "1";
}


function tokenExists() :bool {
    return isset($_SESSION['token']) || isset($_COOKIE['token']);
}

function isFormValid(): bool {
    $login = "user";
    $pass = "pass";

    return $_POST['login'] === $login && $_POST['password'] === $pass;
}

function isFormSubmitted(): bool{
    return isset($_POST['login']) && isset($_POST['password']);
    // equivalent à if (isset($_POST['login']) && isset($_POST['password'])) {return true;} else{ return false;}
}

function displayError() {
    echo '<p style="background: red; color: #990000">STOP wrong credentials</p>';
}

function displayContent() {
    echo '<img src="http://caninest.com/images/loldog.jpg" />';
}


function displayForm(){
    echo '<form action="post.php" method="post">
    <div>
        <label for="login">username</label>
        <input type="text" id="login" name="login">
    </div>
    <div>
        <label for="pass">password</label>
        <input type="password" id="pass" name="password">
    </div>
    <div>
        <label for="remember_me">Remember me</label>
        <input type="checkbox" id="remember_me" name="rememberMe" value="1">
    </div>
    <button>submit</button>
</form>
';
}
