<?php
/**
 * Created by PhpStorm.
 * User: demo
 * Date: 17/12/19
 * Time: 11:24
 * Sert au cours JS sur XHR (ajax)
 */

header('Access-Control-Allow-Origin: http://localhost:63343');

if (getGet('mult1') != null && getGet('mult2') != null) {
    echo getGet('mult1') * getGet('mult2');
}



function getGet($key){
//    return isset($_GET[$key]) ? $_GET[$key] : null;
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }
    else{
        return null;
    }

}
