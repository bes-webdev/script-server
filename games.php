<?php

$data = getData();
displayData($data);




// Functions de la couche MODEL (données)
function getData(): array {
    $db = getDB();
    $request = $db->query('SELECT * FROM jeux_videos');

    return $request->fetchAll();
}

function getDB(): PDO{
    $db = new PDO("mysql:host=127.0.0.1;dbname=demo",'root','root');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Dire à PDO qu'il renvoie les erreurs SQL

    return $db;
}

// Functions de la couche View

function displayData(array $data){
    echo '<table> ';
    echo '<thead><tr>
    <th>id</th>
    <th>nom</th>
    <th>possesseur</th>
    <th>console</th>
    <th>prix</th>
    <th>nombre de joueurs max</th>
    <th>commentaires</th>
    <th>action</th>
</tr></thead>';
    foreach ($data as $jeu) {
        echo '<tr>
        <td>'.$jeu['ID'].'</td>
        <td>'.$jeu['nom'].'</td>
        <td>'.$jeu['possesseur'].'</td>
        <td>'.$jeu['console'].'</td>
        <td>'.$jeu['prix'].'</td>
        <td>'.$jeu['prix'].'</td>
        <td>'.$jeu['nbre_joueurs_max'].'</td>
        <td>'.$jeu['commentaires'].'</td>
        <td><a href="?id='.$jeu['ID'].'">afficher</a></td>
    </tr>';
    }

    echo '</table>';
}




