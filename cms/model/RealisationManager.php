<?php

class RealisationManager
{

    public function getConnection(): PDO{
        $db = new PDO("mysql:host=127.0.0.1;dbname=portfolio","root", "root");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
    }

    public function create(Realisation $realisation): Realisation{
        $db = $this->getConnection();
        $request = $db->prepare("INSERT INTO `realisation` (`title`,`link`,`image`) VALUES (:title, :link, :image)");
        $request->execute([
            'title'=>$realisation->title,
            'link'=>$realisation->link,
            'image'=>$realisation->image,
        ]);

        $realisation->id = $db->lastInsertId();

        return $realisation;
    }

    public function update(Realisation $realisation): Realisation{
        // Requetes sql
    }

    public function delete(Realisation $realisation): void{
        // Requetes sql
    }

    public function getOne(Integer $id): Realisation{
        // Requetes sql
    }

    public function getAll(): Array {
        $db = $this->getConnection();
        $request = $db ->prepare("SELECT * FROM `realisation`");
        $request->execute();
        $result = $request->fetchAll();

        $realisations = [];
        foreach ($result as $line){
            $rea = new Realisation();
            $rea->id = $line['id'];
            $rea->link = $line['link'];
            $rea->title = $line['title'];
            $rea->image = $line['image'];

            $realisations[] = $rea;
        }

        return $realisations;
    }




}