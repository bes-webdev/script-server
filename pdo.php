<?php

$db = new PDO("mysql:host=127.0.0.1;dbname=demo",'root','root');

$request = $db->query('SELECT * FROM eleves');
$eleves = $request->fetchAll(); // fetch All pour recevoir l'ensembe des lignes sous forme d'un tableau

echo '<table> ';
echo '<thead><tr>
    <th>id</th>
    <th>email</th>
    <th>firtName</th>
    <th>lastName</th>
    <th>action</th>
</tr></thead>';
foreach ($eleves as $eleve) {
    echo '<tr>
        <td>'.$eleve['id'].'</td>
        <td>'.$eleve['email'].'</td>
        <td>'.$eleve['firstName'].'</td>
        <td>'.$eleve['lastName'].'</td>
        <td><a href="?id='.$eleve['id'].'">afficher</a></td>
    </tr>';
}

echo '</table>';

if (isset($_GET['id'])){
//    $request = $db->exec('SELECT * FROM eleves WHERE id= '.$_GET["id"]);
    $request = $db->prepare('SELECT * FROM eleves WHERE id= :valeurId');
//    $request->bindParam('id', $_GET['id']);
    $request->execute(['valeurId'=> $_GET['id']]);
    $eleve = $request->fetch(); // Fetch pour recevoir le premier
    if ($eleve !== false){
        echo $eleve['email'];
    }
    else{
        echo 'pas d\'élève trouvé';
    }
}