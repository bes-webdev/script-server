<?php
$filePath = "./files/compteur.txt";
$path = pathinfo($filePath); // Retourne un tableau avec le chemin du fichier décomposé

//si le dossier n'existe pas on le crée
if (!is_dir($path['dirname'])){ // is_dir retourne true ou false en fonction de si le répertoire existe ou pas
    mkdir($path['dirname']); // mkdir crée un répertoire
}
if (!file_exists($filePath)) { // file_exists retourne true ou false si le fichier existe ou pas
    file_put_contents($filePath,0); // file_puts_content crée un fichier si il n'existe pas et mécrit dedans le deuxieme argument
}

$nombreVues = file_get_contents($filePath);
$nombreVues++;
file_put_contents($filePath, $nombreVues);

echo '<h1>Le site a été vu '.$nombreVues.' fois</h1>';
