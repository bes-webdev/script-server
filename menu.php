<?php
$pages = [
    ["menu" => 'Accueil', "title" => 'Bienvenue'],
    ["menu" => 'Contact', "title" => 'Contactez-nous'],
    ["menu" => 'A propos', "title" => 'A propos de nous']
];
$pageId = 0; // Index de la page qui sera affichée
if (isset($_GET['page'])){ // Si on recoit un paramètre page
    $pageId = $_GET['page']; //On écrase la valeur de la page par defaut
}

if (!isset($pages[$pageId])){ //Si la page demandée n'existe pas
    die('Cette page n\'existe pas <a href="?page=0">retour accueil</a>'); // On arrête tout
}

$page = $pages[$pageId]; // On sélectionne la page à afficher


echo '<ul>';
$i=0;
foreach ($pages as $existingPage){
    echo '<li><a href="?page='.$i.'">'.$existingPage['menu'].'</a></li>';
    $i++;
}
echo '</ul>';



echo '<h1>'.$page['title'].'</h1>'; // On affiche le titre de la page