<?php
require_once('model/Realisation.php');
require_once('model/RealisationManager.php');
require_once('view/view.php');

$manager = new RealisationManager();

if (isset($_GET['action'])){
    $action = $_GET['action'];
}else{
    $action = 'list';
}

switch($action) {
    case 'list':
        $realisations = $manager->getAll();
        $content = listRea($realisations);

        break;
    case 'add':

        //Considérons simplement que la requête POST du formulaire n'a pas été envoyée
        $content = reaForm();

        break;
}


//$realisation = new Realisation();
//$realisation->image = 'image';
//$realisation->link = 'http://googel.be';
//$realisation->title = 'Realisation 1';
//
//
//$manager->create($realisation);

page($content);